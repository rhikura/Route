nodejs app.js 2>error.log &
sleep 2
kill -9 `ps h | grep -E 'nodejs app.js' | head -1 | awk '{print $1}'`
